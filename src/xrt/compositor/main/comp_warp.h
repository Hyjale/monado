// Copyright 2020-2021, The Board of Trustees of the University of Illinois.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Compositor spatial reprojection code.
 * @author Jae Lee <jael2@illinois.edu>
 * @ingroup comp_main
 */

#pragma once

#include "vk/vk_helpers.h"
#include "render/comp_render.h"
#include "comp_compositor.h"

#define MESH_WIDTH 64
#define MESH_HEIGHT 64
#define VERTICES_SIZE ((MESH_WIDTH + 1) * (MESH_HEIGHT + 1))
#define INDICES_SIZE (2 * 3 * MESH_WIDTH * MESH_HEIGHT)

struct comp_warp_vertex
{
	float position[3];
	float uv[2];
};

struct comp_warp_ubo_data
{
	struct xrt_matrix_4x4 u_renderInverseP;
	struct xrt_matrix_4x4 u_renderInverseV;
	struct xrt_matrix_4x4 u_warpVP;
	float bleedRadius;
	float edgeTolerance;
};

struct comp_warp
{
	struct vk_bundle vk;

	struct
	{
		struct comp_buffer ubo;
		VkDescriptorSet descriptor_set;
	} views[2];

	struct comp_warp_ubo_data ubo_data[2];

	struct xrt_matrix_4x4 mat_render[2];
	struct xrt_matrix_4x4 mat_projection[2];

	struct comp_buffer vbo;
	struct comp_buffer ibo;

	VkDescriptorPool descriptor_pool;
	VkDescriptorSetLayout descriptor_set_layout;

	VkPipelineLayout pipeline_layout;
	VkPipelineCache pipeline_cache;
	VkPipeline pipeline;

	VkExtent2D extent;

	struct comp_warp_vertex vertices[VERTICES_SIZE];
	uint32_t indices[INDICES_SIZE];

	float nearZ;
	float farZ;

	uint32_t ubo_binding;
	uint32_t color_binding;
	uint32_t depth_binding;
	uint32_t src_binding;
};

struct comp_warp *
comp_warp_create(struct vk_bundle *vk, struct comp_shaders *s, VkExtent2D extent, VkRenderPass render_pass);

void
comp_reproject(struct comp_warp *c, uint32_t i, VkCommandBuffer cmd_buffer, struct xrt_matrix_4x4 mat_world_view);

void
comp_warp_set_projection(struct comp_warp *self, const struct xrt_fov *fov, uint32_t eye);

void
comp_warp_update_descriptor_set(struct vk_bundle *vk,
                                struct comp_warp *c,
                                VkSampler texture_sampler,
                                VkSampler depth_sampler,
                                VkImageView texture_image_view,
                                VkImageView depth_image_view,
                                VkBuffer buffer,
                                VkDeviceSize size,
                                VkDescriptorSet descriptor_set);

void
comp_warp_destroy(struct comp_warp *c);
